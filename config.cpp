class CfgPatches
{
	class dzr_login_control_system_servermod
	{
		requiredAddons[] = {"DZ_Data"};
		units[] = {};
		weapons[] = {};
	};
};

class CfgMods
{
	class dzr_login_control_system_servermod
	{
		type = "mod";
		author = "";
		description = "Advanced whitelist, player db, name registration. Serverside";
		dir = "dzr_login_control_system_servermod";
		name = "DZR Login Control System Serverside";
		//inputs = "dzr_login_control_system_servermod/Data/Inputs.xml";
		dependencies[] = {"Core","Game","World","Mission"};
		class defs
		{
			class engineScriptModule
			{
				files[] = {"dzr_login_control_system_servermod/Common"};
			};
			class gameLibScriptModule
			{
				files[] = {"dzr_login_control_system_servermod/Common"};
			};
			class gameScriptModule
			{
				files[] = {"dzr_login_control_system_servermod/Common", "dzr_login_control_system_servermod/3_Game"};
			};
			class worldScriptModule
			{
				files[] = {"dzr_login_control_system_servermod/Common", "dzr_login_control_system_servermod/4_World"};
			};
			class missionScriptModule
			{
				files[] = {"dzr_login_control_system_servermod/Common", "dzr_login_control_system_servermod/5_Mission"};
			};

		};
	};
};