class DZR_LCS_FilesListing
{
	//ref map<int, ref array<int,int>> text;
	//ext, count, path, map(raw_name, lines_array)
	string extension;
	int count;
	string path;
	ref map<string, array<string> > fileArray;
	void DZR_LCS_FilesListing()
	{
		extension = "";
		count = 0;
		path = "";
		fileArray = new map<string, array<string>>;
		
	}
}

modded class DayZGame
{
	void DayZGame()
	{
		GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(PreinitRPC, 1500, false);
	}
	
	void PreinitRPC()
	{
		GetRPCManager().AddRPC( "DZR_LCS_RPC", "KickWithMessage", this, SingleplayerExecutionType.Client );
	}
	
	void KickWithMessage( CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target )
	{
		Print("KickWithMessage");
		Param2<string, string> data;
		if ( !ctx.Read( data ) || sender != null ) return;
		
		if (type == CallType.Client)
		{
			bool done = false;
			while( !done )
			{
				if (GetDayZGame().GetGameState() != DayZGameState.CONNECT || GetDayZGame().GetGameState() != DayZGameState.CONNECTING || g_Game.GetLoadState() != DayZLoadState.CONNECT_START)
				{
					
					//NotificationSystem.AddNotificationExtended(3.0, data.param1, data.param2, "set:ccgui_enforce image:Icon40Emergency");
					
					GetGame().GetUIManager().ShowDialog(data.param1, data.param2, 1, DBT_OK, DBB_OK, DMT_WARNING, g_Game.GetUIManager().GetMenu());
					GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(GetGame().DisconnectSessionForce, 500, false);
					//ref Param1<PlayerBase> m_Data = new Param1<PlayerBase>(this);
					//GetRPCManager().SendRPC( "DZR_LCS_RPC", "KickMe2", 1, true,sender);
					done = true;
					break;
				}
			}
		}
	}
	/*
		*
		* Name: Steam Name Support
		* Description: Attempts to rid of default use of Survivor name
		by using the player's Steam name.
		*
		* Author: Wardog, wrdg
		* Site: https://wrdg.net/
		*
	*/
	/*
		#ifndef SERVER
		modded class DayZGame
		{
		override bool OnInitialize()
		{
		string name;
		
		GetPlayerName(name);
		name.ToLower(); // case in-sensitive
		
		if (name == "survivor")
		SetPlayerName(GetUniqueName()); // change name if default
		
		return super.OnInitialize(); //! important, call hierarchy last
		}
	*/
	/*
		//? Only use player's Steam name
		override bool OnInitialize()
		{
		SetPlayerName(GetUniqueName()); // change name if default
		return super.OnInitialize(); // call hierarchy last
		}
	*/
	/*
		string GetUniqueName()
		{
		BiosUserManager manager = GetGame().GetUserManager();
		
		if (manager && manager.GetTitleInitiator())
		manager.SelectUserEx(manager.GetTitleInitiator()); // set user as Steam user
		
		if (manager && manager.GetSelectedUser())
		return manager.GetSelectedUser().GetName(); // get Steam name
		
		return GetProfileName(); // almost impossible to get here
		}
		}
		#endif
	*/
}