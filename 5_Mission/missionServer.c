/*	
	void RegisterRPCs()
	{
	GetRPCManager().AddRPC( "RPC_MissionGameplay", "KickClientHandle", this, SingleplayerExecutionType.Client );
	GetRPCManager().AddRPC( "RPC_MissionGameplay", "GetConnectedSession", this, SingleplayerExecutionType.Client );
	}
	void KickClientHandle( CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target )
	{
	Param1<string> data;
	if ( !ctx.Read( data ) || sender != null ) return;
	
	if (type == CallType.Client)
	{
	bool done = false;
	while( !done )
	{
	if (GetDayZGame().GetGameState() != DayZGameState.CONNECT || GetDayZGame().GetGameState() != DayZGameState.CONNECTING || g_Game.GetLoadState() != DayZLoadState.CONNECT_START)
	{
	GetGame().GetUIManager().ShowDialog("#VSTR_SESSION_DISCONNECT", "V++ AdminTools: "+data.param1, 1, DBT_OK, DBB_OK, DMT_WARNING, g_Game.GetUIManager().GetMenu());
	GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(GetGame().DisconnectSessionForce, 100, false);
	done = true;
	break;
	}
	}
	}
	}
*/
//GetRPCManager().AddRPC( "RPC_MissionGameplay", "KickClientHandle", this, SingleplayerExecutionType.Client );
//GetRPCManager().SendRPC( "RPC_MissionGameplay", "KickClientHandle", new Param1<string>( msg ), true, identity);

class LCS_Post_Data 
{
	string username;
	string steamid;
	string ip;
	string secret;
}
class LCS_Response_Data 
{
	string request_username;
	string returned_username;
	string status;
	string requested_steamid;
	string returned_steamid;
}

class LCSDiscordMessage2
{
	string username;
	string content;
	string avatar;
}

void LCS_sendToDiscord(string message, string webhookURL)
{
	//https://discord.com/api/webhooks/1019826931895644171/cP77S_3hhYuykhgt0gHFmb40iBMgw7cg07Z3CfxXLBaVRPYiqV6l9vbsCDl9eYjhU5YY
	
	// Compose the message to be sent to Discord.
	LCSDiscordMessage2 m_LCSDiscordMessage = new LCSDiscordMessage2();
	m_LCSDiscordMessage.username = "Login Control System";
	m_LCSDiscordMessage.content = message;
	m_LCSDiscordMessage.avatar = "https://cdn.discordapp.com/attachments/948492094564089906/948848483157307432/unknown.png";
	
	// Convert that message into JSON format.
	string discordMessageJSON;
	JsonSerializer jsonSerializer = new JsonSerializer();
	jsonSerializer.WriteToString(m_LCSDiscordMessage, false, discordMessageJSON);
	//https://cdn.discordapp.com/attachments/948492094564089906/948848483157307432/unknown.png
	// Post the message to Discord.
	private RestApi restAPI;
	private RestContext restContext;
	webhookURL.Replace("https://discord.com/api/webhooks/", "");
	
	if(GetGame().IsServer())
	{
		restAPI = CreateRestApi();
		restContext = restAPI.GetRestContext("https://discord.com/api/webhooks/");
		restContext.SetHeader("application/json");
		restContext.POST(NULL, webhookURL, discordMessageJSON);
		
		//GetDayZGame().Debug("m_LCSexistingConfig.NotifyOnDiscord: " + m_LCSDiscordMessage.content);
	}
	
}

void LCS_sendToDiscord2(string message, string webhookURL)
{
	//https://discord.com/api/webhooks/1019826931895644171/cP77S_3hhYuykhgt0gHFmb40iBMgw7cg07Z3CfxXLBaVRPYiqV6l9vbsCDl9eYjhU5YY
	
	// Compose the message to be sent to Discord.
	LCSDiscordMessage2 m_LCSDiscordMessage = new LCSDiscordMessage2();
	m_LCSDiscordMessage.username = "Login Control System";
	m_LCSDiscordMessage.content = message;
	m_LCSDiscordMessage.avatar = "https://cdn.discordapp.com/attachments/948492094564089906/948848483157307432/unknown.png";
	
	// Convert that message into JSON format.
	string discordMessageJSON;
	JsonSerializer jsonSerializer = new JsonSerializer();
	jsonSerializer.WriteToString(m_LCSDiscordMessage, false, discordMessageJSON);
	//https://cdn.discordapp.com/attachments/948492094564089906/948848483157307432/unknown.png
	// Post the message to Discord.
	private RestApi restAPI;
	private RestContext restContext;
	webhookURL.Replace("https://discord.com/api/webhooks/", "");
	
	if(GetGame().IsServer())
	{
		restAPI = CreateRestApi();
		restContext = restAPI.GetRestContext("https://discord.com/api/webhooks/");
		restContext.SetHeader("application/json");
		restContext.POST(NULL, webhookURL, discordMessageJSON);
		
		//GetDayZGame().Debug("m_LCSexistingConfig.NotifyOnDiscord: " + m_LCSDiscordMessage.content);
	}
	
}

class MyRestCallback: RestCallback
{
	/**
		\brief Called in case request failed (ERestResultState) - Note! May be called multiple times in case of (RetryCount > 1)
	*/
	override void OnError( int errorCode )
	{
		// override this with your implementation
		//Print(" !!! OnError() "+ errorCode);
	};
	
	/**
		\brief Called in case request timed out or handled improperly (no error, no success, no data)
	*/
	override void OnTimeout()
	{
		// override this with your implementation
		//Print(" !!! OnTimeout() ");
	};
	
	/**
		\brief Called when data arrived and/ or response processed successfully
	*/
	override void OnSuccess( string data, int dataSize )
	{
		// override this with your implementation
		////Print(" !!! OnSuccess() size=" + dataSize );
		//if( dataSize > 0 )
		
		////Print("OnSuccess data:" +data); // !!! NOTE: //Print() will not output string longer than 1024b, check your dataSize !!!
	};
};

modded class MissionServer
{
	/*
		ref dzr_lcs_config_data_class m_LCSdefaultConfig;
		ref dzr_lcs_config_data_class m_LCS_existingConfig;
		const static string dzr_lcs_ProfileFolder = "$profile:\\";
		const static string dzr_lcs_TagFolder = "DZR\\";
		const static string dzr_lcs_ModFolder = "CombatLogDetection\\";
		const static string dzr_lcs_ConfigFile = "dzr_lcs_config.json";	
		static ref dzr_lcs m_DZR_LCS;
	*/
	void MissionServer()
	{
		Print("[dzr_login_control] MissionServer ::: Starting Serverside");
		/*
			if( dzr_lcs_MissionServer_ReadConfigFile() )
			{
			GetDayZGame().Debug("MissionServer start", 0, m_LCS_existingConfig.EnableDebug);
			//Print("[DZR Login Control System] ServerSide ::: ACTIVE (Version: "+m_LCS_existingConfig.ModVersion+"."+m_LCS_existingConfig.ConfigVersion+" DEBUG: "+m_LCS_existingConfig.EnableDebug+")");
			//Print("PenaltyTimer: "+m_LCS_existingConfig.PenaltyTimer);
			//Print("CombatCheckRadius: "+m_LCS_existingConfig.CombatCheckRadius);
			//Print("NotifyOnDiscord: "+m_LCS_existingConfig.NotifyOnDiscord);
			m_DZR_LCS = new dzr_lcs(m_LCS_existingConfig);
			
			
			
			}
			GetDayZGame().Debug("Added RPC dzr_lcs_RPC", 0, m_LCS_existingConfig.EnableDebug);
		*/
		GetRPCManager().AddRPC( "DZR_LCS_RPC", "SetThePlayerName", this, SingleplayerExecutionType.Both );
		GetRPCManager().AddRPC( "DZR_LCS_RPC", "LoadedPlayerStatus", this, SingleplayerExecutionType.Both );
		GetRPCManager().AddRPC( "DZR_LCS_RPC", "KickMe", this, SingleplayerExecutionType.Both );
	}
	
	
	
	void KickMe(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		ref Param1<PlayerBase> data;
		if ( !ctx.Read( data ) )
		{
			//Print("KickMe !ctx.Read( data ): "+data.param1);
			return;	
		};
		if (type == CallType.Server)
        {
            if (data.param1)
            {
				//Print("KickMe ctx.Read( data ): "+data.param1);
				kickPlayer(data.param1, sender, sender.GetId());
				//GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(kickPlayer, 0, false, data.param1, sender, sender.GetId() );
			}
		}
	}
	
	void KickMe2(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		ref Param1<PlayerBase> data;
		if ( !ctx.Read( data ) )
		{
			//Print("KickMe !ctx.Read( data ): "+data.param1);
			return;	
		};
		if (type == CallType.Server)
        {
            if (data.param1)
            {
				//Print("KickMe ctx.Read( data ): "+data.param1);
				kickPlayer(data.param1, sender, sender.GetId());
				//GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(kickPlayer, 0, false, data.param1, sender, sender.GetId() );
			}
		}
	}
	
	void LoadedPlayerStatus(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		//Print("LoadedPlayerStatus CALLED");
		string ResultingStatus;
		string ResultMessageTitle;
		string ResultMessage;
		
		ref Param1<PlayerBase> data;
		if ( !ctx.Read( data ) )
		{
			//Print("LoadedPlayerStatus !ctx.Read( data ): "+data.param1);
			return;	
		};
		if (type == CallType.Server)
		{
			if (data.param1)
			{
				//Print("LoadedPlayerStatus ctx.Read( data ) = "+data.param1);
				ResultingStatus = CheckUser(sender,sender.GetName(),sender.GetPlainId(), data.param1, ResultMessageTitle, ResultMessage);
				// Read config
				string kickDelay_string;
				int kickDelay;
				if ( !checkConfigFile("kickDelay.txt", kickDelay_string)  )
				{
					//Print("[DZR Login Control System] :::  No config file. Create kickDelay.txt in your <server_profile>/DZR/dzr_login_control/ folder.");
					
				}	
				kickDelay = kickDelay_string.ToInt() * 1000;
				// Read config
				
				// Read config
				int ImportantMessageDuration;
				string ImportantMessageDuration_string;
				if ( !checkConfigFile("ImportantMessageDuration.txt", ImportantMessageDuration_string)  )
				{
					//Print("[DZR Login Control System] :::  No config file. Create ImportantMessageDuration.txt in your <server_profile>/DZR/dzr_login_control/ folder. ");
					
				}
				ImportantMessageDuration = ImportantMessageDuration_string.ToInt();
				// Read config
				ref Param1<int> m_Data;
				NotificationSystem.SendNotificationToPlayerIdentityExtended(sender,ImportantMessageDuration, ResultMessageTitle,ResultMessage, "set:ccgui_enforce image:MapShieldBooster" );
				
				//GetRPCManager().SendRPC( "DZR_LCS_RPC", "KickWithMessage", new Param1<string>( msg ), true, identity);
				
				if(ResultingStatus != "NAME_AND_STEAM_MATCH" && ResultingStatus != "IGNORED_NAME")
				{
					m_Data = new Param1<int>(3);	
					GetRPCManager().SendRPC( "DZR_LCS_RPC", "OnPlayerLoadedValidated", m_Data, true, sender);
					
					
				}
				else
				{
					m_Data = new Param1<int>(2);	
					GetRPCManager().SendRPC( "DZR_LCS_RPC", "OnPlayerLoadedValidated", m_Data, true, sender);
				}
			}
		}
	}
	
	
	
	
	/*
		override void InvokeOnConnect(PlayerBase player, PlayerIdentity identity)
		{
		super.InvokeOnConnect(player, identity);
		
		if (!player) { return; }
		
		string steam_id = identity.GetPlainId();
		string player_name = identity.GetName();
		//string player_name = GetCLIParam("connect", param)
		
		
		//ref Param1<PlayerIdentity> m_Data = new Param1<PlayerIdentity>(identity);
		
		//GetRPCManager().SendRPC( "DZR_LCS_RPC", "GetPlayerRealName", m_Data, true, identity);
		
		}
	*/
	void SetThePlayerName(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		//Print("Client tells his real name. Set it.");
		ref Param2<PlayerIdentity, string> data;
		if ( !ctx.Read( data ) )
		{
			return;	
		}
		
		
		if (type == CallType.Server)
		{
			if (data.param1)
			{
				//ref Param2<PlayerIdentity, string> m_Data = new Param2<PlayerIdentity, string>(data.param1, GetCharacterName());
				//Print("Setting name: "+data.param1+" to "+data.param2);
				//GetRPCManager().SendRPC( "DZR_LCS_RPC", "SetThePlayerName", m_Data, true, identity);
				
			}
		}
	}
	
	void kickPlayer(PlayerBase player, PlayerIdentity identity, string uid)
	{
		// Note: At this point, identity can be already deleted
		if (!player)
		{
			//Print("[Logout]: Skipping player " + uid + ", already removed");
			return;
		}
		
		// disable reconnecting to old char
		//GetGame().RemoveFromReconnectCache(uid);
		
		// now player can't cancel logout anymore, so call everything needed upon disconnect
		InvokeOnDisconnect(player);
		
		//Print("[Logout]: Player " + uid + " finished");
		
		if (GetHive())
		{
			// save player
			player.Save();
			
			// unlock player in DB	
			GetHive().CharacterExit(player);		
		}
		
		// handle player's existing char in the world
		player.ReleaseNetworkControls();
		HandleBody(player);
		
		// remove player from server
		GetGame().DisconnectPlayer(identity, uid);
		// Send list of players at all clients
		GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(SyncEvents.SendPlayerList, 1000);
	}
	
	const static string dzr_LCS_ProfileFolder = "$profile:\\";
	const static string dzr_LCS_TagFolder = "DZR\\";
	const static string dzr_LCS_ModFolder = "players\\";
	const static string dzr_LCS_ConfigFile = "dzr_LCS_config.json";	
	
	bool CheckLocalPDB (string name, string steam_id)
	{
		name.ToLower();
		string m_AccessFile = dzr_LCS_ProfileFolder+dzr_LCS_TagFolder+dzr_LCS_ModFolder+"local_player_db\\"+steam_id+"\\server_access.txt";
		string m_NameFile = dzr_LCS_ProfileFolder+dzr_LCS_TagFolder+dzr_LCS_ModFolder+"local_player_db\\"+steam_id+"\\"+name+".name";
		string saved_access;
		string saved_name;
		FileHandle fhandle;
		
		if (FileExist(m_AccessFile)){
			fhandle	=	OpenFile(m_AccessFile, FileMode.READ);
			string line_content;
			while ( FGets( fhandle,  line_content ) > 0 )
			{
				saved_access += line_content;
			}
			//Print("saved_access: "+saved_access);
			CloseFile(fhandle);				
		}
		
		/*
			FileHandle fhandle2;
			if (FileExist(m_NameFile)){
			fhandle2	=	OpenFile(m_NameFile, FileMode.READ);
			//string line_content;
			while ( FGets( fhandle2,  line_content ) > 0 )
			{
			saved_name += line_content;
			}
			//Print("saved_name: "+saved_name);
			CloseFile(fhandle2);				
			}
		*/
		
		if(saved_access == "1" && FileExist(m_NameFile) )
		{
			return true;
		}
		
		return false;
	}
	
	bool CheckLocalAccess (string name, string steam_id)
	{
		name.ToLower();
		string m_AccessFile = dzr_LCS_ProfileFolder+dzr_LCS_TagFolder+dzr_LCS_ModFolder+"local_player_db\\"+steam_id+"\\server_access.txt";
		
		string saved_access;
		
		FileHandle fhandle;
		
		if (FileExist(m_AccessFile)){
			fhandle	=	OpenFile(m_AccessFile, FileMode.READ);
			string line_content;
			while ( FGets( fhandle,  line_content ) > 0 )
			{
				saved_access += line_content;
			}
			//Print("saved_access: "+saved_access);
			CloseFile(fhandle);				
		}
		
		
		if(saved_access == "1" )
		{
			return true;
		}
		
		return false;
	}
	
	void SaveToLocalPlayerDatabase(string name, string steam_id, int value)
	{
		//name.ToLower();
		
		string m_TxtFileName = dzr_LCS_ProfileFolder+dzr_LCS_TagFolder+dzr_LCS_ModFolder+"local_player_db\\"+steam_id+"\\server_access.txt";
		
		string m_NameFile = dzr_LCS_ProfileFolder+dzr_LCS_TagFolder+dzr_LCS_ModFolder+"local_player_db\\"+steam_id+"\\"+name+".name";
		
		if (!FileExist(dzr_LCS_ProfileFolder+dzr_LCS_TagFolder)) MakeDirectory(dzr_LCS_ProfileFolder+dzr_LCS_TagFolder);
		if (!FileExist(dzr_LCS_ProfileFolder+dzr_LCS_TagFolder+dzr_LCS_ModFolder)) MakeDirectory(dzr_LCS_ProfileFolder+dzr_LCS_TagFolder+dzr_LCS_ModFolder);
		if (!FileExist(dzr_LCS_ProfileFolder+dzr_LCS_TagFolder+dzr_LCS_ModFolder+"local_player_db\\"+steam_id)) MakeDirectory(dzr_LCS_ProfileFolder+dzr_LCS_TagFolder+dzr_LCS_ModFolder+"local_player_db\\"+steam_id);
		
		FileHandle fhandle;
		
		fhandle	=	OpenFile(m_TxtFileName, FileMode.WRITE);
		FPrintln(fhandle, value);
		CloseFile(fhandle);
		
		//FileHandle fhandle;
		if(name != "0")
		{
			fhandle	=	OpenFile(m_NameFile, FileMode.WRITE);
			FPrintln(fhandle, name);
			CloseFile(fhandle);
			//Print("В файл "+m_NameFile+" записано значение: " +name);
		}
		
		//Print("В файл "+m_TxtFileName+" записано значение: " +value);
		if (!FileExist(m_TxtFileName) || !FileExist(m_NameFile)) 
		{
			//Print(m_TxtFileName+" или файл имени не был создан.");
			LCS_sendToDiscord(":no_entry: Не получилось сохранить игрока **"+steam_id+"** https://steamcommunity.com/profiles/"+steam_id+" в локальную базу.", "https://discord.com/api/webhooks/1019826931895644171/cP77S_3hhYuykhgt0gHFmb40iBMgw7cg07Z3CfxXLBaVRPYiqV6l9vbsCDl9eYjhU5YY");
		}
		
	}
	
	void DeleteFromLocalPlayerDatabase(string name, string steam_id, int value)
	{
		string m_NameFile = dzr_LCS_ProfileFolder+dzr_LCS_TagFolder+dzr_LCS_ModFolder+"local_player_db\\"+steam_id+"\\"+name+".name";
		DeleteFile( m_NameFile ); 
	}
	
	bool checkConfigFile(string fileName, out string fileContent)
	{
		string m_TxtFileName = "$profile:DZR/dzr_login_control/"+fileName;
		FileHandle fhandle;
		if ( FileExist(m_TxtFileName) )
		{
			//file
			fhandle	=	OpenFile(m_TxtFileName, FileMode.READ);
			//string server_id;
			FGets( fhandle,  fileContent );
			CloseFile(fhandle);
			//Print("[DZR Login Control System] :::  Config "+m_TxtFileName+" is OK! Contents: "+fileContent);
			return true;
		}
		else 
		{
			//Print("[DZR Login Control System] :::  Config "+m_TxtFileName+" is missing!");
			return false;
		}
	}
	
	bool checkPlayerFile(string fileName, out string fileContent)
	{
		string m_TxtFileName = "$profile:DZR/players/local_player_db/"+fileName;
		FileHandle fhandle;
		if ( FileExist(m_TxtFileName) )
		{
			//file
			fhandle	=	OpenFile(m_TxtFileName, FileMode.READ);
			//string server_id;
			FGets( fhandle,  fileContent );
			CloseFile(fhandle);
			//Print("[DZR Login Control System] :::  Config "+m_TxtFileName+" is OK! Contents: "+fileContent);
			return true;
		}
		else 
		{
			//Print("[DZR Login Control System] :::  Config "+m_TxtFileName+" is missing!");
			return false;
		}
	}
	
	
	private TStringArray GetFoldersList()
	{
		////Print("GetFoldersList");
		string	file_name;
		int file_attr;
		int		flags;
		TStringArray list = new TStringArray;
		
		string m_PathToSteamIDs = "$profile:\\DZR\\players\\local_player_db";
		
		string path_find_pattern = m_PathToSteamIDs+"\\*"; //*/
		FindFileHandle file_handler = FindFile(path_find_pattern, file_name, file_attr, flags);
		
		bool found = true;
		while ( found )
		{				
			if ( file_attr == FileAttr.DIRECTORY )
			{
				////Print("GetFoldersList IS DIR: "+file_name);
				list.Insert(file_name);
			}
			
			found = FindNextFile(file_handler, file_name, file_attr);
		}
		
		return list;
	}
	
	bool FilenameExists(string player_name, string steam_id)
	{
		//Print("FilenameExists");
		string folder_name;
		TStringArray m_nameFolders = GetFoldersList();
		string m_PathToSteamIDs = "$profile:\\DZR\\players\\local_player_db";
		//FindFileHandle file_handler = FindFile(m_PathToSteamIDs+"\\*."+extension, file_name, file_attr, flags);
		for ( int i = 0; i < m_nameFolders.Count(); ++i )
		{
			folder_name = m_nameFolders.Get(i);
			//Print("trying to find "+m_PathToSteamIDs+"\\"+folder_name+"\\"+player_name+".name");
			if(FileExist(m_PathToSteamIDs+"\\"+folder_name+"\\"+player_name+".name"))
			{
				
				//Print("steam_id "+steam_id);
				//Print("folder_name "+folder_name);
				
				if(steam_id == folder_name)
				{
					//Print("Legit steam_id for name "+player_name);
					return false;
				}
				else
				{
					//Print("Found "+player_name);
					return true;
				}
			}
		} 
		
		return false;
	}
	
	bool GetFiles(string m_DZRdir, string extension, out DZR_LCS_FilesListing files_return_array)
	{
		string	file_name;
		int 	file_attr;
		int		flags;
		TStringArray list = new TStringArray;
		//"$profile:\\DZR\\"
		string rootDir = "$profile:\\DZR";
		//string path_find_pattern = m_DZRdir+"\\*"; //*/
		string fullPath = rootDir+"\\"+m_DZRdir+"\\";
		//Print("Trying to find all "+extension+" files in "+fullPath);
		FindFileHandle file_handler = FindFile(fullPath+"\\*."+extension, file_name, file_attr, flags);
		
		////Print("file_name: "+file_name);
		
		//ext, count, path, map(raw_name, lines_array)
		files_return_array = new DZR_LCS_FilesListing();
		files_return_array.extension = 	extension;	
		files_return_array.path = 	fullPath;	
		
		bool found = true;
		int counter = 0;
		bool filesExist = false;
		
		FileHandle fhandle;
		while ( found )
		{				
			
			//Print("Found "+extension+" file: "+file_name);
			
			if (FileExist(fullPath+"\\"+file_name))
			{
				filesExist = true;
				fhandle	=	OpenFile(fullPath+"\\"+file_name, FileMode.READ);
				string line_content;
				ref array<string> content = new array<string>;
				while ( FGets( fhandle,  line_content ) > 0 )
				{
					content.Insert(line_content);
				}
				CloseFile(fhandle);
				
				counter = counter + 1;
				files_return_array.fileArray.Insert(file_name, content);
			}
			found = FindNextFile(file_handler, file_name, file_attr);
			files_return_array.count = counter;	
		}
		//Print("counter: "+counter);
		return filesExist;
	}
	
	string FindAllNames(string steam_id)
	{
		
		DZR_LCS_FilesListing files_return_array;
		GetFiles("players\\local_player_db\\"+steam_id, "name", files_return_array);
		
		//if current name matches any .name + server_access 1
		string allNames = "";
		bool validName = false;
		for ( int i = 0; i < files_return_array.fileArray.Count(); i++ )
		{
			string theName = files_return_array.fileArray.GetKey(i);
			
			string theNameNoExt = theName;
			theName.ToLower();
			theNameNoExt.Replace(".name", "");
			//Print("theName: "+theNameNoExt);
			allNames += " "+theNameNoExt;
		}
		return allNames;
	}
	
	
	string CheckUser(PlayerIdentity identity,string player_name,string steam_id, PlayerBase player, out string ResultMessageTitle, out string ResultMessage)
	{
		string m_ReturnStatus;
		//CONFIG
		string controlMode;
		if ( !checkConfigFile("controlMode.txt", controlMode)  )
		{
			//Print("[DZR Login Control System] :::  No config file. Create controlMode.txt in your <server_profile>/DZR/dzr_login_control/ folder and put the mode in it. Modes: remote, local. ");
			//CONFIG
		}
		else
		{
			
			// Read config
			string kickDelay_string;
			int kickDelay;
			if ( !checkConfigFile("kickDelay.txt", kickDelay_string)  )
			{
				//Print("[DZR Login Control System] :::  No config file. Create kickDelay.txt in your <server_profile>/DZR/dzr_login_control/ folder.");
				
			}	
			kickDelay = kickDelay_string.ToInt() * 1000;
			// Read config
			
			if(controlMode == "remote")
			{
				
				//Print("[DZR Login Control System] :::  Remote mode active");
				
				RestContext curlContext;
				
				if(!GetRestApi()){
					CreateRestApi();
					
				};
				
				if (GetRestApi())
				{		
					string receivedData = "NO DATA";
					//string steam_id = data.param2;
					//string player_name = data.param3;
					string error = "";
					GetRestApi().SetOption( 0, 25 );
					GetRestApi().SetOption( 1, 25 );
					GetRestApi().SetOption( 2, 25 );
					
					//TODO: Url to config
					
					// Read config
					string curlContextURL;
					if ( !checkConfigFile("curlContextURL.txt", curlContextURL)  )
					{
						//Print("[DZR Login Control System] :::  No config file. Create curlContextURL.txt in your <server_profile>/DZR/dzr_login_control/ folder. And put your full http:// URL base. webhookURL below will be appended to this base. ");
					}
					// Read config
					
					//string zendz="?secret=BattleServerLastPoint983274239&name="+player_name+"&steam="+steam_id;//+"&ip="+player_ip;
					
					//TODO: Url to config
					//string webhookURL = "/user.php";
					
					// Read config
					string webhookURL;
					if ( !checkConfigFile("webhookURL.txt", webhookURL)  )
					{
						//Print("[DZR Login Control System] :::  No config file. Create webhookURL.txt in your <server_profile>/DZR/dzr_login_control/ folder. And put the URL file (e.g. /user.php ) where we send requests and get results back.");
						
					}
					// Read config
					
					LCS_Post_Data m_LCS_Payload = new LCS_Post_Data();
					
					// Read config
					string server_id;
					if ( !checkConfigFile("server_id.txt", server_id)  )
					{
						//Print("[DZR Login Control System] :::  No config file. Create server_id.txt in your <server_profile>/DZR/dzr_login_control/ folder. And put you secret server id there. Any unique letter combination. ");
						
					}
					// Read config
					
					m_LCS_Payload.secret = server_id;
					m_LCS_Payload.username = player_name;
					m_LCS_Payload.steamid = steam_id;
					
					string lcsPayloadJSON;
					JsonSerializer jsonSerializer = new JsonSerializer();
					jsonSerializer.WriteToString(m_LCS_Payload, false, lcsPayloadJSON);
					
					MyRestCallback cbx1 = new MyRestCallback;
					string player_status;
					curlContext.SetHeader("application/json");
					//curlContext.POST(cbx1, webhookURL, lcsPayloadJSON);
					receivedData = curlContext.POST_now( webhookURL, lcsPayloadJSON);
					//Print("POST_now receivedData:" + receivedData);
					
					LCS_Response_Data response = new LCS_Response_Data;
					jsonSerializer.ReadFromString(response, receivedData, error);
					
					string response_text = "#STR_LCS_TEXT_ERROR";
					
					if(lcsPayloadJSON == receivedData)
					{
						//Start local verification
						//if steamid directory exists = steam found
						//if name.txt exists = registered
						// if requested = local == match
						if( CheckLocalPDB(player_name, steam_id) )
						{
							response_text = "#STR_LCS_NAME_VERIFIED";
							//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,7, player_name, response_text, "set:ccgui_enforce image:MapShieldBooster" );
							
							LCS_sendToDiscord(":warning:  Игрок **"+player_name+"** https://steamcommunity.com/profiles/"+steam_id+" зашёл на сервер через локальную базу игроков. Вероятно внешняя база недоступена.", "https://discord.com/api/webhooks/1019826931895644171/cP77S_3hhYuykhgt0gHFmb40iBMgw7cg07Z3CfxXLBaVRPYiqV6l9vbsCDl9eYjhU5YY");
							
						}
						else	
						{
							response_text = "#STR_LCS_REMOTE_URL_FAIL";
							//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,20, player_name, response_text, "set:ccgui_enforce image:MapShieldBooster" );
							//GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(kickPlayer, kickDelay, false, player, identity, identity.GetId() );
							//kickPlayer(player);
							LCS_sendToDiscord(":no_entry: Игрок **"+player_name+"** https://steamcommunity.com/profiles/"+steam_id+" не смог зайти на сервер, потому что внешня база была недоступна и локальная база игроков в доступе отказала (бан или незареган).", "https://discord.com/api/webhooks/1019826931895644171/cP77S_3hhYuykhgt0gHFmb40iBMgw7cg07Z3CfxXLBaVRPYiqV6l9vbsCDl9eYjhU5YY");
						};
						
					}
					else 
					{
						switch (response.status)
						{
							case "NAME_AND_STEAM_MATCH":
							response_text = "#STR_LCS_NAME_VERIFIED";
							//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,7, response.request_username, response_text, "set:ccgui_enforce image:MapShieldBooster" );
							SaveToLocalPlayerDatabase(response.request_username, steam_id, 1);
							break;
							
							case "LINKED_STEAM_ID":
							case "LINKED_STEAM_ID2":
							LCS_sendToDiscord(":white_check_mark:  Игрок **"+player_name+"** https://steamcommunity.com/profiles/"+steam_id+" зашёл на сервер и его имя было автоматически привязано к Steam ID **"+steam_id+"**", "https://discord.com/api/webhooks/1019826931895644171/cP77S_3hhYuykhgt0gHFmb40iBMgw7cg07Z3CfxXLBaVRPYiqV6l9vbsCDl9eYjhU5YY");
							
							break;
						}
						
						
						
						/*
							string request_username;
							string returned_username;
							string status;
							string requested_steamid;
							string returned_steamid;
						*/
						
						// Read config
						string MismatchURL;
						if ( !checkConfigFile("MismatchURL.txt", MismatchURL)  )
						{
							//Print("[DZR Login Control System] :::  No config file. Create MismatchURL.txt in your <server_profile>/DZR/dzr_login_control/ folder. The game will be shut down and this url will be opened for user in the default browser. And the following GET params will be added to the URL &request_username="+response.request_username+"&returned_username="+response.returned_username+"&status="+response.status+"&requested_steamid="+response.requested_steamid+"&returned_steamid="+response.returned_steamid);
							
						}
						// Read config
						
						ref Param6<string, string, string, string, string, string> m_Data = new Param6<string, string, string, string, string, string>(response.request_username, response.returned_username, response.status, response.requested_steamid, response.returned_steamid, MismatchURL);
						
						if( response.status == "ADMIN_BAN"  || response.status == "ADMIN_BAN2" )
						{
							SaveToLocalPlayerDatabase(response.request_username, steam_id, 0);
						}
						
						
						
						GetRPCManager().SendRPC( "DZR_LCS_RPC", "OpenMismatchURL", m_Data, true, identity);
						
						//TODO
						// SAVE TO SERVERSIDE FOLDER
						
						/*
							curlContext.reset();
							if(player_status != "Timeout")
							{
							//Print(player_status);
							//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,15, "STATUS", player_status );
							GetGame().Chat(player_status, "colorImportant");
							return player_status;
							}
							else
							{
							//Print(player_status);
							//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,15, "STATUS", "Waiting for player verification" );
							};
						*/
					}
				};
				//};
				
			}
			
			if(controlMode == "local")
			{
				
				
				//Print("[DZR Login Control System] :::  LocalDB mode active");
				//process locally
				//CONFIG
				
				string player_name_l = player_name;
				
				player_name_l.ToLower();
				string cfgLanguage;
				if ( !checkConfigFile("Language.txt", cfgLanguage)  )
				{
					//Print("[DZR Login Control System] :::  No config file. Create Language.txt in your <server_profile>/DZR/dzr_login_control/ folder and put En or RU there. ");
				};
				//CONFIG
				
				
				string maximumNamesPerPlayer;
				if ( !checkConfigFile("maximumNamesPerPlayer.txt", maximumNamesPerPlayer)  )
				{
					//Print("[DZR Login Control System] :::  No config file. Create maximumNamesPerPlayer.txt in your <server_profile>/DZR/dzr_login_control/ folder and put the number of allowed names. E.g. 5. It will mean, the player may connect 5 times with different names and all 5 names will be saved as valid names. Connecting with a new 6th name will kick player. ");
				};
				//CONFIG
				
				// Read config
				string WelcomeURL;
				if ( !checkConfigFile("WelcomeURL.txt", WelcomeURL)  )
				{
					//Print("[DZR Login Control System] :::  No config file. Create WelcomeURL.txt in your <server_profile>/DZR/dzr_login_control/ folder. If player joins for first time, this url will be opened for user in the default browser. The following GET params will be added to the URL &request_username="+player_name+"&returned_username="+response.returned_username+"&status="+response.status+"&requested_steamid="+steam_id+"&returned_steamid="+response.returned_steamid+". Leave blank or 0 to disable.");
					
				}			
				// Read config
				
				
				string useSteamNameForUnregistered;
				if ( !checkConfigFile("useSteamNameForUnregistered.txt", useSteamNameForUnregistered)  )
				{
					//Print("[DZR Login Control System] :::  No config file. Create useSteamNameForUnregistered in your <server_profile>/DZR/dzr_login_control/ folder. ");
					
				}
				// Read config	
				
				// Read config
				string MismatchURL2;
				if ( !checkConfigFile("MismatchURL.txt", MismatchURL2)  )
				{
					//Print("[DZR Login Control System] :::  No config file. Create WelcomeURL.txt in your <server_profile>/DZR/dzr_login_control/ folder. If player joins for first time, this url will be opened for user in the default browser. The following GET params will be added to the URL &request_username="+player_name+"&returned_username="+response.returned_username+"&status="+response.status+"&requested_steamid="+steam_id+"&returned_steamid="+response.returned_steamid+". Leave blank or 0 to disable.");
					
				}
				// Read config
				
				// Read config
				int ImportantMessageDuration;
				string ImportantMessageDuration_string;
				if ( !checkConfigFile("ImportantMessageDuration.txt", ImportantMessageDuration_string)  )
				{
					//Print("[DZR Login Control System] :::  No config file. Create ImportantMessageDuration.txt in your <server_profile>/DZR/dzr_login_control/ folder. ");
					
				}
				ImportantMessageDuration = ImportantMessageDuration_string.ToInt();
				// Read config
				
				// Read config
				int WelcomeBackMessageDuration;
				string WelcomeBackMessageDuration_string;
				if ( !checkConfigFile("WelcomeBackMessageDuration.txt", WelcomeBackMessageDuration_string)  )
				{
					//Print("[DZR Login Control System] :::  No config file. Create WelcomeBackMessageDuration.txt in your <server_profile>/DZR/dzr_login_control/ folder. ");
					
				}
				WelcomeBackMessageDuration = WelcomeBackMessageDuration_string.ToInt();
				// Read config
				
				//PlayerIdentity identity,string player_name,string steam_id, PlayerBase player
				//Print("[DZR Login Control System] ::: Checking folder for steam_id "+steam_id);
				// if no steamid folder
				
				//process ignored and illegal
				bool SystemNameMatch = false;
				DZR_LCS_FilesListing ignored_names_array;
				GetFiles("players\\ignoredNames", "name", ignored_names_array);
				for ( int i1 = 0; i1 < ignored_names_array.fileArray.Count(); i1++ )
				{
					string theName1 = ignored_names_array.fileArray.GetKey(i1);
					string theNameNoExt1 = theName1;
					theName1.ToLower();
					theNameNoExt1.Replace(".name", "");
					////Print("theName1: "+theNameNoExt1);
					//allNames += " "+theNameNoExt1;
					if(theName1 == player_name_l+".name")
					{
						SystemNameMatch = true;
						
						//pass player
						// message "Name accepted, welcome"
						if(cfgLanguage == "EN")
						{
							//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,ImportantMessageDuration, player_name, "Name validation ignored. Access granted.", "set:ccgui_enforce image:MapShieldBooster" );
							ResultMessageTitle = player_name;
							ResultMessage = "Name validation ignored. Access granted.";
						}
						else
						{
							//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,ImportantMessageDuration, player_name, "Проверка имени отключена. Доступ разрешён.", "set:ccgui_enforce image:MapShieldBooster" );
							ResultMessageTitle = player_name;
							ResultMessage = "Проверка имени отключена. Доступ разрешён.";
						}
						m_ReturnStatus = "IGNORED_NAME";
					}
				}
				
				
				
				DZR_LCS_FilesListing illegal_names_array;
				GetFiles("players\\illegalNames", "name", illegal_names_array);
				for ( int i2 = 0; i2 < illegal_names_array.fileArray.Count(); i2++ )
				{
					string theName2 = illegal_names_array.fileArray.GetKey(i2);
					string theNameNoExt2 = theName2;
					theName2.ToLower();
					theNameNoExt2.Replace(".name", "");
					////Print("theName2: "+theNameNoExt2);
					//allNames += " "+theNameNoExt2;
					if(theName2 == player_name_l+".name")
					{
						SystemNameMatch = true;
						
						//pass player
						// message "Name accepted, welcome"
						if(cfgLanguage == "EN")
						{
							//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,ImportantMessageDuration, player_name, "This name is forbidden on the server. Use another name.", "set:ccgui_enforce image:MapShieldBooster" );
							ResultMessageTitle = player_name;
							ResultMessage = "This name is forbidden on the server. Use another name.";
						}
						else
						{
							//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,ImportantMessageDuration, player_name, "Запрещенное на сервере имя. Используйте другое.", "set:ccgui_enforce image:MapShieldBooster" );
							ResultMessageTitle = player_name;
							ResultMessage = "Запрещенное на сервере имя. Используйте другое.";
						}
						m_ReturnStatus = "ILLEGAL_NAME";
						//disconnect + //if MismatchURL != "" or 0
						if(MismatchURL2 != "0")
						{
							//Print("Sending OpenMismatchURL");
							ref Param6<string, string, string, string, string, string> m_Data3 = new Param6<string, string, string, string, string, string>(player_name, "", "ILLEGAL_NAME", steam_id, "", MismatchURL2);
							GetRPCManager().SendRPC( "DZR_LCS_RPC", "OpenMismatchURL", m_Data3, true, identity);
						}
						else
						{
							//GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(kickPlayer, kickDelay, false, player, identity, identity.GetId() );
						}
						// open url
					}
				}
				
				
				//process ignored and illegal
				
				
				bool nameTaken = false;
				int reCount;
				
				//CHECK TAKEN
				
				nameTaken = false;
				if(FilenameExists(player_name, steam_id) & !SystemNameMatch)
				{
					string allNames2;
					nameTaken = true;
					allNames2 = FindAllNames(steam_id);
					m_ReturnStatus = "STEAMID_ALREADY_LINKED";
					if(MismatchURL2 != "0")
					{
						//Print("Sending OpenMismatchURL");
						ref Param6<string, string, string, string, string, string> m_Data_FilenameExists = new Param6<string, string, string, string, string, string>(player_name, "", "STEAMID_ALREADY_LINKED", steam_id, "", MismatchURL2);
						GetRPCManager().SendRPC( "DZR_LCS_RPC", "OpenMismatchURL", m_Data_FilenameExists, true, identity);
					}
					else
					{
						
						if(cfgLanguage == "EN")
						{
							if(allNames2 == "")
							{
								//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,ImportantMessageDuration, "This name is taken", "This name '"+player_name+"' is already bound to another Steam ID. Please set another name.", "set:ccgui_enforce image:MapShieldBooster" );
								ResultMessageTitle = "This name is taken";
								ResultMessage = "This name '"+player_name+"' is already bound to another Steam ID. Please set another name.";
							}
							else
							{
								//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,ImportantMessageDuration, "This name is taken", "This name '"+player_name+"' is already bound to another Steam ID. Please set another name. Or use your previously saved names: "+allNames2 , "set:ccgui_enforce image:MapShieldBooster" );
								ResultMessageTitle = "This name is taken";
								ResultMessage = "This name '"+player_name+"' is already bound to another Steam ID. Please set another name. Or use your previously saved names: "+allNames2;
							}
						}
						else
						{
							if(allNames2 == "")
							{
								//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,ImportantMessageDuration, "Имя занято", "Имя '"+player_name+"' уже привязано к другому Steam ID. Выберите другое имя." , "set:ccgui_enforce image:MapShieldBooster" );
								ResultMessageTitle = "Имя занято";
								ResultMessage = "Имя '"+player_name+"' уже привязано к другому Steam ID. Выберите другое имя.";
							}
							else
							{
								//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,ImportantMessageDuration, "Имя занято", "Имя '"+player_name+"' уже привязано к другому Steam ID. Выберите другое имя. Или используйте сохранённые ранее имена: "+allNames2 , "set:ccgui_enforce image:MapShieldBooster" );
								ResultMessageTitle = "Имя занято";
								ResultMessage = "Имя '"+player_name+"' уже привязано к другому Steam ID. Выберите другое имя. Или используйте сохранённые ранее имена: "+allNames2;
							}
						}
						
						//GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(kickPlayer, kickDelay, false, player, identity, identity.GetId() );
					}
					
					
				}
				
				//CHECK TAKEN
				
				if(!SystemNameMatch && !nameTaken)
				{
					
					
					
					if (!FileExist(dzr_LCS_ProfileFolder+dzr_LCS_TagFolder+dzr_LCS_ModFolder+"local_player_db\\"+steam_id) )
					{
						
						// Get player name and SteamID
						DZR_LCS_FilesListing files_return_array;
						
						// Create a folder with steamid
						// Save to a file
						// create server_access 1 file
						MakeDirectory(dzr_LCS_ProfileFolder+dzr_LCS_TagFolder+dzr_LCS_ModFolder+"local_player_db\\"+steam_id);	
						//Print("::: [DZR Login Control System] :: Creating folder: "+dzr_LCS_ProfileFolder+dzr_LCS_TagFolder+dzr_LCS_ModFolder+"local_player_db\\"+steam_id);
						SaveToLocalPlayerDatabase(player_name, steam_id, 1);
						
						GetFiles("players\\"+"local_player_db\\"+steam_id, "name", files_return_array);
						
						reCount = maximumNamesPerPlayer.ToInt() - files_return_array.count;
						//Print( "One more name saved for your steamid. Available name slots: "+ reCount );
						// create folders: give_gear_once + items_list.txt display_once_join_messages permanent_join_messages
						// pass player and send a message that the name is saved and available name slots: maximumNamesPerPlayer-1
						
						if(cfgLanguage == "EN")
						{
							//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,ImportantMessageDuration, "Welcome to the server", "Your Steam ID "+steam_id+" is now linked to this name: "+player_name+". Use it to join server. You used 1 of the "+maximumNamesPerPlayer+" name slots. Available name slots left: "+ reCount , "set:ccgui_enforce image:MapShieldBooster" );
							ResultMessageTitle = "Welcome to the server";
							ResultMessage = "Your Steam ID "+steam_id+" is now linked to this name: "+player_name+". Use it to join server. You used 1 of the "+maximumNamesPerPlayer+" name slots. Available name slots left: "+ reCount;
						}
						else
						{
							//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,ImportantMessageDuration, "Добро пожаловать на сервер!", "Ваш стимайди "+steam_id+" привязан теперь к этому имени: "+player_name+". Заходите на сервер с ним. Использовано слотов для имени: 1 из "+maximumNamesPerPlayer+". Слотов для имени доступно: "+ reCount , "set:ccgui_enforce image:MapShieldBooster" );
							ResultMessageTitle = "Добро пожаловать на сервер!";
							ResultMessage = "Ваш стимайди "+steam_id+" привязан теперь к этому имени: "+player_name+". Заходите на сервер с ним. Использовано слотов для имени: 1 из "+maximumNamesPerPlayer+". Слотов для имени доступно: "+ reCount;
						}
						m_ReturnStatus = "NAME_AND_STEAM_MATCH";
						//if WelcomeURL != "" or 0
						// open url
						// GetRPCManager().SendRPC( "DZR_LCS_RPC", "OpenMismatchURL", m_Data, true, identity);
						// if WelcomeGear.txt !=0
						//spawn gear
						// message WelcomeGearMessage.txt
					} 
					else
					{
						//if exists
						//Print("::: [DZR Login Control System] :: Folder exists: "+dzr_LCS_ProfileFolder+dzr_LCS_TagFolder+dzr_LCS_ModFolder+"local_player_db\\"+steam_id);
						//GetNameFiles()
						//find all .name files and count
						GetFiles("players\\local_player_db\\"+steam_id, "name", files_return_array);
						
						//if current name matches any .name + server_access 1
						string allNames = "";
						bool validName = false;
						for ( int i = 0; i < files_return_array.fileArray.Count(); i++ )
						{
							string theName = files_return_array.fileArray.GetKey(i);
							
							string theNameNoExt = theName;
							theName.ToLower();
							theNameNoExt.Replace(".name", "");
							//Print("theName: "+theNameNoExt);
							allNames += " "+theNameNoExt;
							if(theName == player_name_l+".name")
							{
								nameTaken = true;
								
								//pass player
								// message "Name accepted, welcome"
								
								//ManualBan
								if(!CheckLocalAccess(player_name, steam_id))
								{
									SystemNameMatch = true;
									m_ReturnStatus = "ADMIN_BAD";
									if(cfgLanguage == "EN")
									{
										//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,ImportantMessageDuration, "No Access for this Steam ID", "Admin has restricted acces for Steam ID "+steam_id, "set:ccgui_enforce image:MapShieldBooster" );
										ResultMessageTitle = "No Access for this Steam ID";
										ResultMessage ="Admin has restricted acces for Steam ID "+steam_id;
									}
									else
									{
										//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,ImportantMessageDuration, "Доступ закрыт", "Админ ограничил доступ к серверу для Steam ID "+steam_id, "set:ccgui_enforce image:MapShieldBooster" );
										ResultMessageTitle = "Доступ закрыт";
										ResultMessage = "Админ ограничил доступ к серверу для Steam ID "+steam_id;
									}
									
									//disconnect + //if MismatchURL != "" or 0
									if(MismatchURL2 != "0")
									{
										//Print("Sending OpenMismatchURL");
										ref Param6<string, string, string, string, string, string> m_Data4 = new Param6<string, string, string, string, string, string>(player_name, "", "ADMIN_BAN", steam_id, "", MismatchURL2);
										GetRPCManager().SendRPC( "DZR_LCS_RPC", "OpenMismatchURL", m_Data4, true, identity);
									}
									else
									{
										//GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(kickPlayer, kickDelay, false, player, identity, identity.GetId() );
									}	
								} 
								else
								{
									m_ReturnStatus = "NAME_AND_STEAM_MATCH";
									validName = true;
									if(cfgLanguage == "EN")
									{
										//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,WelcomeBackMessageDuration, "Your name is valid", "Welcome back, "+player_name+"!", "set:ccgui_enforce image:MapShieldBooster" );
										ResultMessageTitle = "Your name is valid";
										ResultMessage = "Welcome back, "+player_name+"!";
									}
									else
									{
										//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,WelcomeBackMessageDuration, "Имя игрока проверено", "С возвращением, "+player_name+"!", "set:ccgui_enforce image:MapShieldBooster" );
										ResultMessageTitle = "Имя игрока проверено";
										ResultMessage = "С возвращением, "+player_name+"!";
									}
									
								}
								//ManualBan
								
								
							}
						}
						
						
						//if no matches
						//check maximumNamesPerPlayer
						//if 0 
						if(!nameTaken)
						{
							reCount = maximumNamesPerPlayer.ToInt() - files_return_array.count;
							if(reCount <= 0)
							
							{
								
								m_ReturnStatus = "NOT_REGISTERED";
								if(cfgLanguage == "EN")
								{
									//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,ImportantMessageDuration, "No more slots", "You have used all name slots. Use one of your saved names: "+allNames, "set:ccgui_enforce image:MapShieldBooster" );
									ResultMessageTitle = "No more slots";
									ResultMessage = "You have used all name slots. Use one of your saved names: "+allNames;
								}
								else
								{
									//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,ImportantMessageDuration, "Не осталось слотов для имён", "Вы использовали все слоты для новых имён. Используйте ваши сохранённые имена: "+allNames, "set:ccgui_enforce image:MapShieldBooster" );
									ResultMessageTitle = "Не осталось слотов для имён";
									ResultMessage = "Вы использовали все слоты для новых имён. Используйте ваши сохранённые имена: "+allNames;
								}
								
								//disconnect + //if MismatchURL != "" or 0
								if(MismatchURL2 != "0")
								{
									//Print("Sending OpenMismatchURL");
									ref Param6<string, string, string, string, string, string> m_Data2 = new Param6<string, string, string, string, string, string>(player_name, "", "NOT_REGISTERED", steam_id, "", MismatchURL2);
									GetRPCManager().SendRPC( "DZR_LCS_RPC", "OpenMismatchURL", m_Data2, true, identity);
									//GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(kickPlayer, kickDelay, false, player, identity, identity.GetId() );
								}
								else
								{
									//GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(kickPlayer, kickDelay, false, player, identity, identity.GetId() );
								}
								// open url
								
							}
							else
							{
								//if fileCount >= maximumNamesPerPlayer
								//
								// if maximumNamesPerPlayer >= filecount
								// massage "Name limit reached. Use one of the following names: "
								//else
								reCount = reCount - 1;
								
								SaveToLocalPlayerDatabase(player_name, steam_id, 1);
								////Print( "One more name saved for your steamid. Available name slots: "+ reCount );
								m_ReturnStatus = "NAME_AND_STEAM_MATCH";
								if(cfgLanguage == "EN")
								{
									//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,ImportantMessageDuration, "New name saved: "+player_name, "One more name saved for your steamid. Available name slots: "+ reCount, "set:ccgui_enforce image:MapShieldBooster" );
									ResultMessageTitle = "New name saved: "+player_name;
									ResultMessage = "One more name saved for your steamid. Available name slots: "+ reCount;
								}
								else
								{
									//NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,ImportantMessageDuration, "Сохранено новое имя: "+player_name, "Для вашего Steam ID сохранено ещё одно имя. Осталось слотов для имён: "+ reCount, "set:ccgui_enforce image:MapShieldBooster" );
									ResultMessageTitle = "Сохранено новое имя: "+player_name;
									ResultMessage = "Для вашего Steam ID сохранено ещё одно имя. Осталось слотов для имён: "+ reCount;
								}
								
								// save another .name file
								
								
								
							}
						}
					}	
					
					
					
				}
			}
			
		};
		return m_ReturnStatus;
	};
	/*
		override void OnChatCommand(PlayerIdentity sender, string cmd, TStringArray args) {
		
		//PrefixGroup AdminPrefixGroup = GetAdminPrefixGroup();
		
		//Admin Commands
		if(true) {
		TStringArray args = new TStringArray();
		cmd.Split(" ", args);
		cmd = args[0];
		arg1 = args[1];
		arg2 = args[2];
		switch(cmd) {
		// Add or Remove admin tag
		case "addname":
		SendSimpleChatMessage(sender, "adding name: "+arg1+" = "+arg2);
		default:
		super.OnChatCommand(sender, cmd, args);
		}
		} else {
		//Player commands
		switch(cmd) {
		case "remove name":
		SendSimpleChatMessage(sender, "Removing name: "+arg1+" = "+arg2);
		super.OnChatCommand(sender, cmd, args);
		default:
		super.OnChatCommand(sender, cmd, args);
		}
		}
		}
	*/
	
	void CheckAccess(PlayerIdentity identity)
	{
		string ResultingStatus;
		string ResultMessageTitle;
		string ResultMessage;
		
		//Print("LoadedPlayerStatus ctx.Read( data ) = "+data.param1);
		ResultingStatus = CheckUser(identity,identity.GetName(),identity.GetPlainId(), NULL, ResultMessageTitle, ResultMessage);
		Print(ResultingStatus);
		Print(ResultMessageTitle);
		Print(ResultMessage);
		// Read config
		string kickDelay_string;
		int kickDelay;
		if ( !checkConfigFile("kickDelay.txt", kickDelay_string)  )
		{
			//Print("[DZR Login Control System] :::  No config file. Create kickDelay.txt in your <server_profile>/DZR/dzr_login_control/ folder.");
			
		}	
		kickDelay = kickDelay_string.ToInt() * 1000;
		// Read config
		
		// Read config
		int ImportantMessageDuration;
		string ImportantMessageDuration_string;
		if ( !checkConfigFile("ImportantMessageDuration.txt", ImportantMessageDuration_string)  )
		{
			//Print("[DZR Login Control System] :::  No config file. Create ImportantMessageDuration.txt in your <server_profile>/DZR/dzr_login_control/ folder. ");
			
		}
		ImportantMessageDuration = ImportantMessageDuration_string.ToInt();
		// Read config
		//ref Param1<int> m_Data;
		//NotificationSystem.SendNotificationToPlayerIdentityExtended(sender,ImportantMessageDuration, ResultMessageTitle,ResultMessage, "set:ccgui_enforce image:MapShieldBooster" );
		
		
		
		if(ResultingStatus != "NAME_AND_STEAM_MATCH" && ResultingStatus != "IGNORED_NAME")
		{
			Print("KickWithMessage");
			GetRPCManager().SendRPC( "DZR_LCS_RPC", "KickWithMessage", new Param2<string,string>( ResultMessageTitle, ResultMessage ), true, identity);					
		}
		else
		{
			NotificationSystem.SendNotificationToPlayerIdentityExtended(identity,ImportantMessageDuration, ResultMessageTitle,ResultMessage, "set:ccgui_enforce image:MapShieldBooster" );
		}
		
		//GetRPCManager().SendRPC( "RPC_MissionGameplay", "KickClientHandle", new Param1<string>( msg ), true, identity);
		
	}
	
	override void OnEvent(EventType eventTypeId, Param params) {
		super.OnEvent(eventTypeId,params);
		
		PlayerIdentity identity;
        PlayerBase     player;
		
        switch(eventTypeId)
        {
            //Called when player joins/respawns (PREP STAGE)
            case ClientPrepareEventTypeID:
            {
				Print("ClientPrepareEventTypeID");
				
                ClientPrepareEventParams clientPrepareParams;
                Class.CastTo(clientPrepareParams, params);
				
                identity = clientPrepareParams.param1;
				//CheckAccess(identity, eventTypeId);
                break;
			}
			//Called when a saved player joins (READY STAGE)
            case ClientReadyEventTypeID:
            {
				Print("ClientReadyEventTypeID");
				
				ClientReadyEventParams readyParams;
                Class.CastTo(readyParams, params);
                
                identity = readyParams.param1;
                player   = PlayerBase.Cast(readyParams.param2);
				CheckAccess(identity);
				break;
			}
			
			//Called when player joins(new character)/respawns (2nd PREP STAGE)
			
			
			case ClientNewReadyEventTypeID:
            {
				Print("ClientNewReadyEventTypeID");
				
                ClientNewReadyEventParams newReadyParams; //PlayerIdentity, Man
                Class.CastTo(newReadyParams, params);
				
                identity = newReadyParams.param1;
                player   = PlayerBase.Cast(newReadyParams.param2);
				CheckAccess(identity);
				break;
			}
			
			case ClientReconnectEventTypeID:
            {
				Print("ClientReconnectEventTypeID");
				break;
			}
			
		}
		
		
		if(eventTypeId == ChatMessageEventTypeID ) {
			
			//Man c_player = GetGame().GetPlayer();
			
			//PlayerBase player = ( PlayerBase ) GetGame().GetPlayer();
			
			
			
			
			
			//PlayerBase cc_player = PlayerBase.Cast( c_player )
			////Print("Player valid:" + cc_player);	
			//string steamid = cc_player.GetIdentity().GetPlainId()
			
			
			ChatMessageEventParams chat_params = ChatMessageEventParams.Cast( params );
			//string message = chat_params.param3;
			
			//GetDayZGame().GetFelixBotAPI().newChatMessage(message, chat_params.param2);
			
			
			
			if (chat_params.param3 != "") //trigger only when channel is Global == 0, Player Name does not equal to null and entered command
			{
				TStringArray args = new TStringArray();
				chat_params.param3.Split(" ", args);
				string cmd = args[0];
				string arg1 = args[1];
				string arg2 = args[2];
				int arg3 = args[3].ToInt();
				string arg4 = args[4];
				
				// Read config
				string adminPass;
				bool IsAdmin = false;
				if ( checkConfigFile("adminPass.txt", adminPass)  )
				{
					//Print("adminPass: "+adminPass);
					//Print("arg4: "+arg4);
					if(adminPass == arg4)
					{
						//addname 76561198000801794 Nommer 1 123
						//removename 76561198000801794 Nommer 1 123
						IsAdmin = true;
					}
				}
				// Read config
				if (IsAdmin)
				{
					switch(cmd) {
						// Add or Remove admin tag
						case "an":
						//Print("adding name: "+arg1+" = "+arg2);
						SaveToLocalPlayerDatabase(arg2, arg1, arg3);
						break;
						case "rn":
						//Print("removing name: "+arg1+" = "+arg2);
						DeleteFromLocalPlayerDatabase(arg2, arg1, arg3);
						break;
						
					}
				}
				
			}
			
			
		}
	}
	
	/*
		override PlayerBase OnClientNewEvent(PlayerIdentity identity, vector pos, ParamsReadContext ctx)
		{
		string characterType;
		string characterName;
		//m_RespawnMode = GetGame().ServerConfigGetInt("setRespawnMode"); //todo - init somewhere safe
		//SyncRespawnModeInfo(identity);
		// get login data for new character
		//Print("CREATING NEW CHARACTER");
		//DeserializeCharacterData(ctx);
		
		if ( ProcessLoginData(ctx) && (m_RespawnMode == GameConstants.RESPAWN_MODE_CUSTOM) && !GetGame().GetMenuDefaultCharacterData(false).IsRandomCharacterForced() )
		{
		if (GetGame().ListAvailableCharacters().Find(GetGame().GetMenuDefaultCharacterData().GetCharacterType()) > -1)
		{
		characterType = GetGame().GetMenuDefaultCharacterData().GetCharacterType();
		characterName = GetGame().GetMenuDefaultCharacterData().GetCharacterName();
		}
		else 
		{
		characterType = GetGame().CreateRandomPlayer();
		}
		}
		else
		{
		characterType = GetGame().CreateRandomPlayer();
		GetGame().GetMenuDefaultCharacterData().GenerateRandomEquip();
		}
		
		if (CreateCharacter(identity, pos, ctx, characterType, characterName))
		{
		EquipCharacter(GetGame().GetMenuDefaultCharacterData());
		}
		//Print("characterType:"+characterType);
		//Print("characterName:"+characterName);
		//Print("GetGame().GetMenuDefaultCharacterData(false).IsRandomCharacterForced():"+GetGame().GetMenuDefaultCharacterData(false).IsRandomCharacterForced());
		return m_player;
		};
		
		PlayerBase CreateCharacter(PlayerIdentity identity, vector pos, ParamsReadContext ctx, string characterType, string characterName)
		{
		Entity playerEnt;
		playerEnt = GetGame().CreatePlayer(identity, characterType, pos, 0, characterName);//Creates random player
		Class.CastTo(m_player, playerEnt);
		
		GetGame().SelectPlayer(identity, m_player);
		
		return m_player;
		}
		
		bool DeserializeCharacterData(ParamsReadContext ctx)
		{
		string m_CharacterType;
		string m_CharacterName;
		string m_AttachmentsMap;
		bool m_ForceRandomCharacter;
		
		if (ctx.Read(m_CharacterType))
		//Print("m_CharacterType: "+m_CharacterType);
		if (ctx.Read(m_AttachmentsMap))
		//Print("m_AttachmentsMap: "+m_AttachmentsMap);
		if (ctx.Read(m_ForceRandomCharacter))
		//Print("m_ForceRandomCharacter: "+m_ForceRandomCharacter);
		if (ctx.Read(m_CharacterName))
		//Print("m_CharacterName: "+m_CharacterName);
		
		//DumpAttMapContents();
		return true;
		}
	*/
	/*
	bool ProcessLoginData(ParamsReadContext ctx)
	{
		//ParamsReadContext LoginData;
		//creates temporary server-side structure for handling default character spawn
		GetGame().GetMenuDefaultCharacterData(false).DeserializeCharacterData(ctx);
		
		
		
		
		string data;
		if (ctx.Read(data))
		{
			//Print("data: "+data);
		}
		
		return GetGame().GetMenuDefaultCharacterData(false).DeserializeCharacterData(ctx);
	}
	*/
}									